<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\User;
use Hash;
use Tests\TestCase;

class LoginApiTest extends TestCase
{
    use RefreshDatabase;

    public function createUser()
    {
        User::create([
            'email' => 'admin@admin.com',
            'name' => 'admin',
            'password' => Hash::make('password'),
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login_api_field_is_required_validation()
    {
        $response = $this->json('POST', 'http://localhost:8000/api/login')
        ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    'email' => ["The email field is required."],
                    'password' => ["The password field is required."],
                ]
            ]);
    }

    public function test_login_api_email_validation()
    {
        $response = $this->json('POST', 'http://localhost:8000/api/login', [
            'email' => 'dsfdsfdsfdsfdsfds',
            'password' => '121231232'
        ])
        ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    'email' => ["The email must be a valid email address."],
                ]
            ]);
    }

    public function test_checkIfEmailExistInDatabase()
    {
        $this->createUser();

        $this->assertDatabaseHas('users', ['email' => 'admin@admin.com']);
    }

    public function test_check_valid_login()
    {
        $this->createUser();
        $response = $this->json('POST', 'http://localhost:8000/api/login', [
            'email' => 'admin@admin.com',
            'password' => 'password',
        ])
        ->assertStatus(200)
            ->assertJson([
                "message" => "Success",
            ]);
    }

    public function test_check_invalid_login()
    {
        $this->createUser();
        $response = $this->json('POST', 'http://localhost:8000/api/login', [
            'email' => 'admin@admin.com',
            'password' => 'password121212',
        ])
        ->assertStatus(422)
            ->assertJson([
                "message" => "Invalid email or password",
            ]);
    }

    public function test_check_new_user_create()
    {
        $response = $this->json('POST', 'http://localhost:8000/api/create', [
            'email' => 'admin2@demo.com',
            'name' => 'admin',
            'password' => 'password',
        ])
        ->assertStatus(200)
        ->assertJson([
            "message" => "Success",
        ]);
        $this->assertDatabaseHas('users', ['email' => 'admin2@demo.com']);
    }
}
