<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class ApiController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email'=>'required|email|exists:users',
            'password'=>'required'
        ]);

        $User = User::whereEmail($request->email)->first();
        if (!$User || !Hash::check($request->password, $User->password)) {
            return response()->json(['message'=>'Invalid email or password'], 422);
        }

        return response()->json([
            'message'=>'Success',
            'user'=> $User
        ], 200);
    }

    public function create(Request $request)
    {
        $request['password'] = Hash::make($request->password);
        User::create($request->all());
        return response()->json([
            'message'=>'Success',
        ], 200);
    }
}
